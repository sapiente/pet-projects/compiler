from Token import *


class Lexer(object):
    def __init__(self, text):
        self.text = text
        self.pos = 0
        self.current_char = self.text[self.pos]

    def error(self):
        raise Exception('Error parsing input')

    # move position and read next char
    def next_char(self):
        self.pos += 1
        if self.pos > len(self.text) - 1:
            self.current_char = None
        else:
            self.current_char = self.text[self.pos]

    # skip white spaces inside text
    def skip_whitespaces(self):
        if self.current_char is not None and self.current_char.isspace():
            self.next_char()

    # read whole integer from text
    def integer(self):
        result = ''
        while self.current_char is not None and self.current_char.isdigit():
            result += self.current_char
            self.next_char()

        return int(result)

    # read next token from text
    def get_next_token(self):

        while self.current_char is not None:
            if self.current_char.isspace():
                self.skip_whitespaces()
                continue

            if self.current_char.isdigit():
                return Token(INTEGER, self.integer())

            if self.current_char == '-':
                self.next_char()
                return Token(MINUS, '-')

            if self.current_char == '+':
                self.next_char()
                return Token(PLUS, '+')

            if self.current_char == '*':
                self.next_char()
                return Token(MUL, '*')

            if self.current_char == '/':
                self.next_char()
                return Token(DIV, '/')

            if self.current_char == '(':
                self.next_char()
                return Token(LPAREN, '(')

            if self.current_char == ')':
                self.next_char()
                return Token(RPAREN, ')')

        return Token(EOF, None)