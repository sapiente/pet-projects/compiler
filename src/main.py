from Interpreter import *
from Lexer import *
from Parser import *


def main():
    while True:
        try:
            text = input('calc> ')
        except EOFError:
            break
        if not text:
            continue

        interpreter = Interpreter(Parser(Lexer(text)))
        print(interpreter.interpret())

if __name__ == '__main__':
    main()
